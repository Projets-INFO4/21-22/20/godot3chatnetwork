extends Control

onready var chat_setup_ui = $Chat_setup
onready var server_ip_address = $Chat_setup/server_ip
onready var chat_ui = $Chat
onready var message = $Chat/chat_input
onready var chatlist = $Chat/chatlist
onready var pseudo = $Chat_setup/name
onready var disp = $addr/ip_address_show

var player_info = {}
var info = {}

func _ready() -> void:
	get_tree().connect("network_peer_connected", self, "_player_connected")
	get_tree().connect("network_peer_disconnected", self, "_player_disconnected")
	disp.text = Network.ip_address
	


func _set_name():
	if pseudo.text != "":
		info =  {name=pseudo.text}
	else:
		info = {name="user"}

func _on_create_server_pressed():
	chat_setup_ui.hide()
	_set_name()
	Network.create_server()
	chat_ui.show()


func _on_join_server_pressed():
	if server_ip_address.text != "":
		chat_setup_ui.hide()
		Network.ip_address = server_ip_address.text
		_set_name()
		Network.join_server()
		chat_ui.show()
		
	
func _player_connected(id) -> void:
	rpc_id(id, "register_player", info)
	print("Player " + str(id) + " has connected")

remote func register_player(info):
	# Get the id of the RPC sender.
	var id = get_tree().get_rpc_sender_id()
	# Store the info
	player_info[id] = info

remote func send(message):
	chatlist.text = chatlist.text + message + "\n"

func _player_disconnected(id) -> void:
	print("Player " + str(id) + " has disconnected")


func _on_send_pressed():
	if message.text != null:
		var msg = info.name + " : " + message.text
		rpc("send", msg)
		chatlist.text = chatlist.text + msg + "\n"
		
